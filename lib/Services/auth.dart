import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class AuthBase {
  User? get currentUser;
  Future<User?> guestSignIn();
  Future<void> signOut();
  Stream<User?> authStateChanges();
  Future<User?> googleSignin();
  Future<User?> facebookSignIn();
}

class Auth implements AuthBase {
  final _firebaseAuth = FirebaseAuth.instance;

  @override
  Stream<User?> authStateChanges() => _firebaseAuth.authStateChanges();

  @override
  User? get currentUser => _firebaseAuth.currentUser;

  @override
  Future<User?> facebookSignIn() async {
    final fb = FacebookLogin();
    final response = await fb.logIn(
      permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email,
      ],
    );
    switch (response.status) {
      case FacebookLoginStatus.success:
        final accessToken = response.accessToken;
        final userCredential = await _firebaseAuth.signInWithCredential(
          FacebookAuthProvider.credential(accessToken!.token),
        );
        return userCredential.user;
      case FacebookLoginStatus.cancel:
        throw FirebaseAuthException(
          code: "SIGNIN_CANCELED_BY_USER",
          message: "User cancelled login",
        );
      case FacebookLoginStatus.error:
        throw FirebaseAuthException(
          code: "ERROR_SIGNING_IN_FACEBOOK",
          message: response.error?.developerMessage,
        );
      default:
        throw UnimplementedError();
    }
  }

  @override
  Future<User?> googleSignin() async {
    final googleSignin = GoogleSignIn();
    final googleUser = await googleSignin.signIn();
    if (googleUser != null) {
      final googleAuth = await googleUser.authentication;
      if (googleAuth.idToken != null) {
        final userCredentials = await _firebaseAuth
            .signInWithCredential(GoogleAuthProvider.credential(
          idToken: googleAuth.idToken,
          accessToken: googleAuth.accessToken,
        ));
        return userCredentials.user;
      } else {
        throw (FirebaseAuthException(
          code: 'ERROR_MISSING_ID_TOKEN',
          message: 'Missing token',
        ));
      }
    } else {
      throw (FirebaseAuthException(
        code: 'ERROR_ABORTED_BY_USER',
        message: "User aborted google sigin",
      ));
    }
  }

  @override
  Future<User?> guestSignIn() async {
    final userCredentials = await _firebaseAuth.signInAnonymously();
    return userCredentials.user;
  }

  @override
  Future<void> signOut() async {
    final googleSignIn = GoogleSignIn();
    final fbLogin = FacebookLogin();
    await fbLogin.logOut();
    await googleSignIn.signOut();
    await _firebaseAuth.signOut();
  }
}
