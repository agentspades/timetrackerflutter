import 'package:flutter/material.dart';
import 'package:time_tracker/Pages/Sign_In/email_signin_page.dart';
import 'package:time_tracker/Pages/Sign_In/sign_in_button.dart';
import 'package:time_tracker/Services/auth.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key, required this.auth}) : super(key: key);
  final AuthBase auth;

  Future<void> _guestSignIn() async {
    try {
      auth.guestSignIn();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _googleSignIn() async {
    try {
      auth.googleSignin();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _facebookSignIn() async {
    try {
      auth.facebookSignIn();
    } catch (e) {
      print(e.toString());
    }
  }

  void _emailSignin(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        // fullscreenDialog: true, // IOS specific code (screen comes up from bottom and has an 'x')
        builder: (context) => const EmailSigninPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Time Tracker"),
        elevation: 2.0,
        backgroundColor: Colors.grey[600],
      ),
      body: _buildPage(context),
      backgroundColor: Colors.grey[500],
    );
  }

  Padding _buildPage(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            "Sign In",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: "WhiteDream",
                fontSize: 30.0,
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 40.0),
          SignInButton(
            text: "Sign in with Google",
            textColor: Colors.black,
            color: Colors.white,
            image: 'assets/images/google-logo.png',
            onPressed: _googleSignIn,
          ),
          const SizedBox(height: 8.0),
          SignInButton(
            text: "Sign in with FaceBook",
            color: const Color(0xFF334D92),
            textColor: Colors.white,
            image: 'assets/images/facebook-logo.png',
            onPressed: _facebookSignIn,
          ),
          const SizedBox(height: 8.0),
          SignInButton(
            text: "Sign in with Email",
            color: Colors.green[400],
            textColor: Colors.white,
            image: 'assets/images/email-logo.png',
            onPressed: () => _emailSignin(context),
          ),
          const SizedBox(height: 8.0),
          const Text(
            "or",
            style: TextStyle(color: Colors.black54, fontSize: 15.0),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 8.0),
          SignInButton(
            text: "Sign in as Guest",
            color: Colors.deepOrange,
            textColor: Colors.white,
            image: 'assets/images/anon-logo.png',
            onPressed: _guestSignIn,
          ),
        ],
      ),
    );
  }
}
