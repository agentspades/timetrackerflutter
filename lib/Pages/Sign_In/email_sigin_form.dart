// ignore_for_file: prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';

class EmailSIgnInForm extends StatelessWidget {
  EmailSIgnInForm({Key? key}) : super(key: key);

  final bool _isButtonEnabled = true;

  List<Widget> _formControls() {
    return [
      const TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: "Email",
          labelStyle: TextStyle(
            color: Colors.white,
          ),
          hintText: "penus@abc.com",
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white,
            ),
          ),
        ),
      ),
      const SizedBox(height: 5.0),
      const TextField(
        obscuringCharacter: '\$',
        obscureText: true,
        decoration: InputDecoration(
          labelText: "Password",
          labelStyle: TextStyle(
            color: Colors.white,
          ),
          hintText: "Enter your Password",
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white,
            ),
          ),
        ),
      ),
      const SizedBox(height: 20.0),
      ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(7.0),
              ),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
            Colors.deepPurple,
          ),
        ),
        onPressed: _isButtonEnabled ? () {} : null,
        child: const Text(
          "Sign In",
          style: TextStyle(
            letterSpacing: 1.2,
            fontSize: 18.0,
          ),
        ),
      ),
      TextButton(
        onPressed: () {},
        child: const Text(
          "Need an account? Register Here!",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: _formControls(),
      ),
    );
  }
}
