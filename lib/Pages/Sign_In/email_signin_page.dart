import 'package:flutter/material.dart';
import 'package:time_tracker/Pages/Sign_In/email_sigin_form.dart';

class EmailSigninPage extends StatelessWidget {
  const EmailSigninPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Sign in with Email"),
        elevation: 2.0,
        backgroundColor: Colors.grey[600],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: SingleChildScrollView(
            child: Card(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(15.0),
                ),
              ),
              child: EmailSIgnInForm(),
            ),
          ),
        ),
      ),
      backgroundColor: Colors.grey[500],
    );
  }
}
