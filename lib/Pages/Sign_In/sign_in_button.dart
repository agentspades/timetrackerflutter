import 'package:flutter/material.dart';
import 'package:time_tracker/Widgets/customelevatedButton.dart';

class SignInButton extends CustomElevatedButton {
  SignInButton({
    Key? key,
    required String text,
    Color? color,
    Color? textColor,
    required VoidCallback onPressed,
    double height = 50.0,
    double fontSize = 16.0,
    required String image,
  }) : super(
            key: key,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image(
                  image: AssetImage(image),
                  height: 30.0,
                ),
                Text(text,
                    style: TextStyle(
                      color: textColor,
                      fontSize: fontSize,
                    )),
                Opacity(
                  opacity: 0.0,
                  child: Image.asset(image),
                ),
              ],
            ),
            color: color,
            onPressed: onPressed,
            borderRadius: 12.0,
            height: height);
}
