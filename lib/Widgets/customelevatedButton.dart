// ignore_for_file: file_names

import 'package:flutter/material.dart';

class CustomElevatedButton extends StatelessWidget {
  const CustomElevatedButton({
    Key? key,
    required this.child,
    this.color,
    this.borderRadius = 5.0,
    required this.onPressed,
    this.height = 30,
  }) : super(key: key);

  final Widget child;
  final Color? color;
  final double borderRadius;
  final VoidCallback onPressed;
  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: ElevatedButton(
        onPressed: onPressed,
        child: child,
        style: ButtonStyle(
            // ref: https://stackoverflow.com/a/65763949
            overlayColor: MaterialStateProperty.resolveWith(
              (states) {
                return states.contains(MaterialState.pressed)
                    ? Colors.black45
                    : null;
              },
            ),
            backgroundColor: MaterialStateProperty.all<Color?>(color),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.all(Radius.circular(borderRadius))))),
      ),
    );
  }
}
