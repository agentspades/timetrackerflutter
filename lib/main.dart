import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:time_tracker/Pages/Landing_Page/landing_page.dart';
import 'package:time_tracker/Services/auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Time Tracker",
      themeMode: ThemeMode.dark,
      darkTheme: ThemeData.dark(),
      home: LandingPage(
        auth: Auth(),
      ),
      debugShowCheckedModeBanner: true,
    );
  }
}
